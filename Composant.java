/**
 * Created by m.najafi on 10/18/2017.
 */
////Exercice 5.3:

public abstract class Composant {
    String nom;
    int taille;
    Composant parent;

    public Composant(String nom, int taille) {
        this.nom = nom;
        this.taille = taille;
    }

    public int getTaille() {
        return taille;
    }

    public boolean isAncetre(Composant composant){
        if(parent == null) return  false;
        if(parent == composant) return true;
        return parent.isAncetre(composant);
    }
}

public class Fichier extends Composant {
    public Fichier(String nom, int taille) {
        super(nom, taille);
    }

    public int taille() {
        return super.getTaille();
    }
}

import java.util.Vector;

public class Repertoire extends Composant {
    Vector<Composant> repertoire;

    public Repertoire(String nom) {
        super(nom,0);
        repertoire = new Vector<Composant>();
    }

    public void ajout(Composant composant) throws Exception {
        if (this.equals(composant)) throw new Exception("On ne peut pas ajouter un repertoire à lui même");
        if(isAncetre(composant))throw new Exception("un repertoire ne peux pas etre un descedant de lui même");
        composant.parent = this;
        repertoire.add(composant);
        taille += composant.taille;
    }

    public void retirer(Composant composant) {
        repertoire.remove(composant);
        taille -= composant.taille;
        composant.parent = null;
    }

    public int calcule_taille() {
        return super.getTaille();
    }
}

}



