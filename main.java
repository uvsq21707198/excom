/**
 * Created by m.najafi on 10/18/2017.
 */
public class Main {


    public static void main(String[] args) throws Exception {

        Repertoire racine = new Repertoire("/");
        Repertoire app = new Repertoire("app");
        Fichier f1 = new Fichier("f1",100);
        Fichier f2 = new Fichier("f1",100);
        Fichier f3 = new Fichier("f1",100);
        System.out.println(f1.taille());
        racine.ajout(app);
        racine.ajout(f1);
        racine.ajout(f2);
        racine.ajout(f3);
        System.out.println(racine.calcule_taille());
        racine.retirer(f1);
        System.out.println(racine.calcule_taille());
        app.ajout(racine);
    }